import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    Todos:[
    {
        task: "Buy New House",
        userId: 1,
        id: 1,
        title: "delectus aut autem",
        completed: false,
        left: 12
    },
    {
        task: "Buy New Car",
        userId: 1,
        id: 2,
        title: "quis ut nam facilis et officia qui",
        completed: false,
        left: 0
    },
    {
        task: "Buy New Company",
        userId: 1,
        id: 3,
        title: "fugiat veniam minus",
        completed: false,
        left: 1
    },
    {
        task: "Buy Car Wash",
        userId: 1,
        id: 4,
        title: "et porro tempora",
        completed: false,
        left: 41
    },
    {
        task: "Buy Car Gas",
        userId: 1,
        id: 5,
        title: "laboriosam mollitia et enim quasi adipisci quia provident illum",
        completed: false,
        left: 22
    },
    {
        task: "Buy Car Wheels",
        userId: 1,
        id: 6,
        title: "qui ullam ratione quibusdam voluptatem quia omnis",
        completed: false,
        left: 21
    },
    {
        task: "Buy Car Vacuum Cleaner",
        userId: 1,
        id: 7,
        title: "illo expedita consequatur quia in",
        completed: false,
        left: 4
    },
    {
        task: "Buy Wireles Headphones",
        userId: 1,
        id: 13,
        title: "et doloremque nulla",
        completed: false,
        left: 3
    },
    {
        task: "Build Dream Car",
        userId: 1,
        id: 14,
        title: "repellendus sunt dolores architecto voluptatum",
        completed: false,
        left: 2
    },
    {
        task: "Build New Plane",
        userId: 1,
        id: 15,
        title: "ab voluptatum amet voluptas",
        completed: false,
        left: 8
    },
    {
        task: "Repair Phone",
        userId: 1,
        id: 16,
        title: "accusamus eos facilis sint et aut voluptatem",
        completed: false,
        left: 6
    },
    {
        task: "Build Dream Car",
        userId: 1,
        id: 17,
        title: "quo laboriosam deleniti aut qui",
        completed: false,
        left: 4
    },
    {
        task: "Build Busines Plan",
        userId: 1,
        id: 18,
        title: "dolorum est consequatur ea mollitia in culpa",
        completed: false,
        left: 2
    },
    {
        task: "Build Car",
        userId: 1,
        id: 19,
        title: "molestiae ipsa aut voluptatibus pariatur dolor nihil",
        completed: false,
        left: 0
    },
    {
        task: "Buy Tesla",
        userId: 1,
        id: 20,
        title: "ullam nobis libero sapiente ad optio sint",
        completed: false,
        left: 0
    },
    {
        task: "Repair Watches",
        userId: 2,
        id: 21,
        title: "suscipit repellat esse quibusdam voluptatem incidunt",
        completed: false,
        left: 0
    },
    {
        task: "Buy Keys",
        userId: 2,
        id: 22,
        title: "distinctio vitae autem nihil ut molestias quo",
        completed: false,
        left: 1
    },
    {
        task: "Buy Water",
        userId: 2,
        id: 23,
        title: "et itaque necessitatibus maxime molestiae qui quas velit",
        completed: false,
        left: 1
    },
    {
        task: "Build Dream Car",
        userId: 2,
        id: 24,
        title: "adipisci non ad dicta qui amet quaerat doloribus ea",
        completed: false,
        left: 23
    },
    {
        task: "Repair Cahrger",
        userId: 2,
        id: 25,
        title: "voluptas quo tenetur perspiciatis explicabo natus",
        completed: false,
        left: 1
    },
    {
        task: "Buy Chair",
        userId: 2,
        id: 26,
        title: "aliquam aut quasi",
        completed: false,
        left: 26
    },
    {
        task: "Buy Iphone 13",
        userId: 2,
        id: 27,
        title: "veritatis pariatur delectus",
        completed: false,
        left: 29
    },
    {
        task: "Buy New Cleaner Machine ",
        userId: 2,
        id: 28,
        title: "nesciunt totam sit blanditiis sit",
        completed: false,
        left: 12
    },
    {
        task: "Build Tasks",
        userId: 2,
        id: 29,
        title: "laborum aut in quam",
        completed: false,
        left: 6
    },
    {
        task: "Get New Salary",
        userId: 2,
        id: 30,
        title: "nemo perspiciatis repellat ut dolor libero commodi blanditiis omnis",
        completed: false,
        left: 12
    },
    {
        task: "Open New Shops",
        userId: 2,
        id: 31,
        title: "repudiandae totam in est sint facere fuga",
        completed: false,
        left: 52
    },
    {
        task: "Buy Chocolate",
        userId: 2,
        id: 32,
        title: "earum doloribus ea doloremque quis",
        completed: false,
        left: 21
    },
    {
        task: "Get New FeedBack",
        userId: 2,
        id: 33,
        title: "sint sit aut vero",
        completed: false,
        left: 1 
    },
    {
        task: "See through time",
        userId: 2,
        id: 34, 
        title: "porro aut necessitatibus eaque distinctio",
        completed: false,
        left: 31
    },
    {
        task: "Build Car",
        userId: 2,
        id: 35,
        title: "repellendus veritatis molestias dicta incidunt",
        completed: false,
        left: 1 
    }
],
    Today: [],
    Tomorrow: [],
    Later: []
  },
  getters:{
   Todos: state => state.Todos,
   Today: state => state.Today,
   Tomorrow: state => state.Tomorrow,
   Later: state => state.Later,
   Date:(state) =>state.Date.toLocalString("eng", {month:"long"}).toString().slice(0,3)

  },
  mutations: {
  FILLTER_ALL(state){
    // Меняем state только через mutation
    state.Today = state.Todos.filter(item => item.left == 0)
    state.Tomorrow = state.Todos.filter(item => item.left == 1)
    state.Later = state.Todos.filter(item => item.left > 1)
    console.log(state.Later);
    console.log(state.Tomorrow);
    console.log(state.Today);
      },
  },
  actions: {
   SORTED_ARRAYS({commit}){
     // commit -- вызов мутации
     //мутация - меняет.state
     commit("FILLTER_ALL")
   }
  },
})
