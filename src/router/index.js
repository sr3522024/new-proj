import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'
import OnlyToday from '../views/OnlyToday.vue'


Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/OnlyToday',
    name: 'OnlyToday',
    component: OnlyToday
  },
]

const router = new VueRouter({
  routes
})

export default router
